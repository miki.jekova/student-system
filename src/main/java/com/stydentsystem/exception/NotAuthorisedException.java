package com.stydentsystem.exception;

/**
 * This is a custom exception that is thrown when user is trying to perform an action
 * for which he has not been authorized.
 */

public class NotAuthorisedException extends Exception {
    public NotAuthorisedException(String type) {
        super(String.format("%s", type));
    }

}
