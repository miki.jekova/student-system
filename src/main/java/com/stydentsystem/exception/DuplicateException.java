package com.stydentsystem.exception;

/**
 * This is a custom exception that is thrown when trying to add
 * an entity that already exists in the database.
 */

public class DuplicateException extends Exception {

    public DuplicateException(String type, String attribute, String name) {
        super(String.format("%s with %s %s already exists!", type, attribute, name));
    }
}