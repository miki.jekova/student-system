package com.stydentsystem.exception;

/**
 * This is a custom exception that is thrown when trying to find an entity
 * that does not exist in the database.
 */

public class NotFoundException extends Exception {
    public NotFoundException(String type, String id, String attribute) {
        super(String.format("%s with %s %s was not found in the Repository!",
                type, id, attribute));
    }

    public NotFoundException(String type, String attribute) {
        super(String.format
                ("%s with %s was not found!", type, attribute));
    }

    public NotFoundException(String type){
        super(String.format("%s", type));
    }
}
