package com.stydentsystem.controller;

import com.stydentsystem.service.contract.CourseService;
import com.stydentsystem.service.contract.StudentService;
import com.stydentsystem.service.contract.TeacherService;
import com.stydentsystem.service.contract.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * This is a class that maps model attributes relating to the admin role of the user class
 * to the view.
 */

@Controller
@RequestMapping("/admin")
public class AdminController {
    UserService userService;
    CourseService courseService;
    TeacherService teacherService;
    StudentService studentService;

    public AdminController(UserService userService, CourseService courseService,
                           TeacherService teacherService, StudentService studentService) {
        this.userService = userService;
        this.courseService = courseService;
        this.teacherService = teacherService;
        this.studentService = studentService;
    }

    @GetMapping
    public String showAdminPage(Model model, Principal principal) {
        try {
            model.addAttribute("user", userService.getByUsername(principal.getName()));
            model.addAttribute("courses", courseService.getAll());
        } catch (Exception e) {
            return "redirect:/";
        }
        return "admin";
    }

    @GetMapping("/teachers")
    public String showAllTeachers(Model model) {
        try {
            model.addAttribute("teachers", teacherService.getAll());
        } catch (Exception e) {
            return "redirect:/";
        }
        return "teachers-list";
    }


    @GetMapping("/courses")
    public String showAllCourses(Model model) {
        try {
            model.addAttribute("courses", courseService.getAll());
        } catch (Exception e) {
            return "redirect:/";
        }
        return "courses-list";
    }

    @GetMapping("/students")
    public String showAllStudents(Model model) {
        try {
            model.addAttribute("students", studentService.getAll());
        } catch (Exception e) {
            return "redirect:/";
        }
        return "students-list";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }

    @GetMapping("/users")
    public String getAllUsers(Model model, Principal principal) {
        try {
            model.addAttribute("user", userService.getByUsername(principal.getName()));
            model.addAttribute("users", userService.getAll());
        } catch (Exception e) {
            return "redirect:/logout";
        }
        return "admin-users";
    }
}