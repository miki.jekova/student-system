package com.stydentsystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * This is a class that visualise the login form which is used to authenticate the users.
 */

@Controller
public class LoginController {
    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }
}
