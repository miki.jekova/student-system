package com.stydentsystem.controller;

import com.stydentsystem.service.contract.CourseService;
import com.stydentsystem.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This is a class that visualise home page.
 */

@Controller
@RequestMapping("/")
public class HomeController {

    private CourseService courseService;

    private UserService userService;

    @Autowired
    public HomeController(CourseService courseService, UserService userService) {
        this.courseService = courseService;
        this.userService = userService;
    }

 /*   @GetMapping("/course/{id}")
    public String showCourse(@PathVariable("id") String courseId, Model model) {
        Course course = new Course();
        course = courseService.getCourseById(courseId);
        model.addAttribute("course", course);
        model.addAttribute("creator", course.getCreator().getName());

        return "index";
    }*/
}


