package com.stydentsystem.controller;

import com.stydentsystem.exception.NotFoundException;
import com.stydentsystem.entity.AppUser;
import com.stydentsystem.entity.dto.UserDTO;
import com.stydentsystem.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * This is a class that provides the capability of the user to register in the current application.
 * In order to use the additional functionality visible only for registered user.
 */

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegistrationPage(Model model) {
        model.addAttribute("user", new UserDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") UserDTO userDTO, BindingResult bindingResult, Model model) throws NotFoundException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Credentials cannot be empty!");
            return "register";
        }

        if (userDetailsManager.userExists(userDTO.getUsername())) {
            model.addAttribute("error", "User with the same username already exists!");
            return "register";
        }

        if (!userDTO.getPassword().equals(userDTO.getPasswordConfirmation())) {
            model.addAttribute("error", "Password doesn't match");
            return "register";
        }
        saveUser(userDTO);
        return "register-confirmation";
    }

    private void saveUser(@ModelAttribute("user") @Valid UserDTO userDTO) throws NotFoundException {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        User newUser = new User(userDTO.getUsername(), passwordEncoder.encode(userDTO.getPassword()), authorities);
        userDetailsManager.createUser(newUser);

        AppUser additionalInfoUser = new AppUser(userDTO.getName(), userDTO.getEmail());
        com.stydentsystem.entity.User user = userService.getByUsername(newUser.getUsername());
        user.setAppUser(additionalInfoUser);
        userService.updateUser(user, additionalInfoUser);
    }

    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }
}
