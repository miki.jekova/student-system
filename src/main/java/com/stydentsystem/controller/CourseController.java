package com.stydentsystem.controller;

import com.stydentsystem.entity.Course;
import com.stydentsystem.exception.NotFoundException;
import com.stydentsystem.service.contract.CourseService;
import com.stydentsystem.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * This is a class that visualise course page.
 */
@Controller
@RequestMapping("/courses")
public class CourseController {

    private CourseService courseService;
    private UserService userService;

    @Autowired
    public CourseController(CourseService courseService, UserService userService) {
        this.courseService = courseService;
        this.userService = userService;
    }

    @GetMapping
    public String showCourses(Model model, Principal principal) throws NotFoundException {
        getNamePrincipal(model, principal);
        model.addAttribute("courses", courseService.getAll());
        return "courses";
    }

    private void getNamePrincipal(Model model, Principal principal) throws NotFoundException {
        if (principal != null) {
            model.addAttribute("user", userService.getByUsername(principal.getName()));
        }
    }
    
    @GetMapping("/{id}")
    public String showCourseById(@PathVariable int id, Model model, Principal principal) throws NotFoundException {
        Course course = courseService.getCourseById(id);
            if (principal.getName().equals(course.getCreator())) {
                model.addAttribute("creator", course.getCreator());
            }

        model.addAttribute("course", course);
        return "course";
    }

}
