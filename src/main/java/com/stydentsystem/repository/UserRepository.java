package com.stydentsystem.repository;

import com.stydentsystem.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * This interface serves to retrieve database records for the User class.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByIdAndEnabledIsTrue(int id);

    User findByUsernameAndEnabledIsTrue(String username);

    List<User> findAllByEnabledIsTrue();

    boolean existsByUsername(String username);

    boolean existsByIdAndEnabledIsTrue(int id);

    boolean existsByUsernameAndEnabledIsTrue(String username);
}

