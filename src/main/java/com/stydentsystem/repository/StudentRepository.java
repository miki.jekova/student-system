package com.stydentsystem.repository;


import com.stydentsystem.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * This interface serves to retrieve database records for the Student class.
 */
public interface StudentRepository extends JpaRepository<Student, Integer> {

    List<Student> findAll();
}