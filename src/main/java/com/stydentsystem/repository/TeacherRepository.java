package com.stydentsystem.repository;

import com.stydentsystem.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * This interface serves to retrieve database records for the Teacher class.
 */
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

    List<Teacher> findAll();

}
