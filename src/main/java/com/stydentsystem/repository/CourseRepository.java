package com.stydentsystem.repository;

import com.stydentsystem.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * This interface serves to retrieve database records for the Course class.
 */
public interface CourseRepository extends JpaRepository<Course, Integer> {

    List<Course> findAll();

    Course findCourseById(int id);
}
