package com.stydentsystem.repository;

import com.stydentsystem.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * This interface serves to retrieve database records for the User's additional details class.
 */
public interface AppUserRepository extends JpaRepository<AppUser, Integer> {

    AppUser findById(int id);

    List<AppUser> findAll();
}

