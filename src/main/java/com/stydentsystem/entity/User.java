package com.stydentsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * This class is main part of the project. With it we can create/update/delete users.
 * We have different kinds of users: Admin/Registered/Anonymous.
 * Anonymous/Registered/Admin can browse/filter/sort addons.
 * Registered/Admin can create/update/delete addons.
 * Admin can approve new addons. Also can delete users.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "enabled=1")
@Table(name = "users")
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "username")
    @Size(min = 3, max = 30, message = "Username must be between 3 and 30 characters!")
    private String username;

    @Size(min = 6, max = 64)
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @OneToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;
}

