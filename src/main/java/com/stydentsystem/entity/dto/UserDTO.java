package com.stydentsystem.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

/**
 * This class is used to transfer data to create an User class object.
 * The data is provided by аn anonymous user or admin.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    @Size(min = 3, max = 30, message = "Username must be between 3 and 30 characters!")
    private String username;

    @Size(min = 3, max = 30, message = "Password must be between 3 and 30 characters!")
    private String password;

    private String passwordConfirmation;

    @Size(min = 2, max = 50, message = "Name must be between 2 and 50 characters!")
    private String name;

    @NotEmpty(message = "Email cannot be null!")
    private String email;
}

