package com.stydentsystem.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
/**
 * This class is main part of the project. With it we can create/update/delete teacher.
 */
@Entity
@Table(name = "teachers")
@NoArgsConstructor
@Getter
@Setter
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="name")
    private String name;

    @OneToMany(mappedBy = "creatorId", targetEntity = Course.class)
    @Column(name="courses")
    private List<Course> createdCourses;
    // id, name, created couses

    public Teacher(String name, List<Course> createdCourses) {
        this.name = name;
        this.createdCourses = createdCourses;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                '}';
    }
}
