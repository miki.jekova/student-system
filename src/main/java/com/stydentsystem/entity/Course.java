package com.stydentsystem.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
/**
 * This class is main part of the project. With it we can create/update/delete course.
 */
@Entity
@Table(name = "courses")
@NoArgsConstructor
@Getter
@Setter
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="name")
    private String name;

    @ManyToMany(targetEntity = Student.class)
    @Column(name="students")
    private List<Student> students;

    @Column(name="creator")
    private String creator;

    @Column(name="creator_id")
    private int creatorId;

    public Course(String name, String creator, int creatorId) {
        this.name = name;
        this.creator = creator;
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", creator=" + creator +
                '}';
    }
}

