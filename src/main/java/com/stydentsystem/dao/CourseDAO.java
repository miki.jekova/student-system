package com.stydentsystem.dao;

import com.stydentsystem.configuration.SessionFactoryUtil;
import com.stydentsystem.entity.Course;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.List;
/**
 * This class is used for CRUD operations for the course.
 */
public class CourseDAO {

    public static void saveCourse(Course course) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(course);
        transaction.commit();
        session.close();
    }

    public static Course getCourse(long id) {
        Course course;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        course = session.get(Course.class, id);
        transaction.commit();
        return course;
    }

    public static void updateCourse(Course course) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(course);
        transaction.commit();
        session.close();
    }

    public static void deleteCourse(Course course) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(course);
        transaction.commit();
        session.close();
    }

    public static void saveCourses(Collection<Course> courseSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        courseSet.forEach(session::save);
        transaction.commit();
        session.close();
    }

    public static List<Course> getCourses() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT c FROM Course c", Course.class).getResultList();
    }

    public static void updateCourses(Collection<Course> courseSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        courseSet.forEach(session::update);
        transaction.commit();
        session.close();
    }

    public static void deleteCourses(Collection<Course> courseSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        courseSet.forEach(session::delete);
        transaction.commit();
        session.close();
    }
}
