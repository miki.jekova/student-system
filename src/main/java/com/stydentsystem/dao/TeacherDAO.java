package com.stydentsystem.dao;

import com.stydentsystem.configuration.SessionFactoryUtil;
import com.stydentsystem.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.List;
/**
 * This class is used for CRUD operations for the teacher.
 */
public class TeacherDAO {

    public static void saveTeacher(Teacher teacher) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(teacher);
        transaction.commit();
        session.close();
    }

    public static Teacher getTeacher(long id) {
        Teacher teacher;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        teacher = session.get(Teacher.class, id);
        transaction.commit();
        return teacher;
    }

    public static void updateTeacher(Teacher teacher) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(teacher);
        transaction.commit();
        session.close();
    }

    public static void deleteTeacher(Teacher teacher) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(teacher);
        transaction.commit();
        session.close();
    }

    public static void saveTeachers(Collection<Teacher> teacherSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        teacherSet.forEach(session::save);
        transaction.commit();
        session.close();
    }

    public static List<Teacher> getTeachers() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT t FROM Teacher t", Teacher.class).getResultList();
    }

    public static void updateTeachers(Collection<Teacher> teacherSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        teacherSet.forEach(session::update);
        transaction.commit();
        session.close();
    }

    public static void deleteTeachers(Collection<Teacher> teacherSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        teacherSet.forEach(session::delete);
        transaction.commit();
        session.close();
    }
}
