package com.stydentsystem.dao;

import com.stydentsystem.configuration.SessionFactoryUtil;
import com.stydentsystem.entity.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.List;

/**
 * This class is used for CRUD operations for the student.
 */
public class StudentDAO {
    
    public static void saveStudent(Student student) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(student);
        transaction.commit();
        session.close();
    }

    public static Student getStudent(long id) {
        Student student;
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        student = session.get(Student.class, id);
        transaction.commit();
        return student;
    }

    public static void updateStudent(Student student) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(student);
        transaction.commit();
        session.close();
    }

    public static void deleteStudent(Student student) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(student);
        transaction.commit();
        session.close();
    }

    public static void saveStudents(Collection<Student> studentSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        studentSet.forEach(session::save);
        transaction.commit();
        session.close();
    }

    public static List<Student> getStudents() {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("SELECT s FROM Student s", Student.class).getResultList();
    }

    public static void updateStudents(Collection<Student> studentSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        studentSet.forEach(session::update);
        transaction.commit();
        session.close();
    }

    public static void deleteStudents(Collection<Student> studentSet) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        studentSet.forEach(session::delete);
        transaction.commit();
        session.close();
    }
}
