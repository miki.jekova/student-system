package com.stydentsystem;

import com.stydentsystem.dao.CourseDAO;
import com.stydentsystem.dao.TeacherDAO;
import com.stydentsystem.entity.Course;
import com.stydentsystem.entity.Teacher;

import java.util.Collections;

/**
 * Entry point used for CRUD operation with DAO classes.
 * To help with populating data in the database.
 */

public class EntryPoint {
    public static void main(String[] args) {
        Teacher teacher = new Teacher("Bob2", Collections.emptyList());
        TeacherDAO.saveTeacher(teacher);

        Course course = new Course("Math2", teacher.getName(), teacher.getId());
        CourseDAO.saveCourse(course);

        System.out.flush();
    }
}
