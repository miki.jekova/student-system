package com.stydentsystem.service;

import com.stydentsystem.entity.Course;
import com.stydentsystem.repository.CourseRepository;
import com.stydentsystem.service.contract.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * This is a class that provides implementation of the main functionality of the course.
 */
@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    @Override
    public List<Course> getAll() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourseById(int id) {
        return courseRepository.findCourseById(id);
    }
}
