package com.stydentsystem.service;

import com.stydentsystem.entity.AppUser;
import com.stydentsystem.entity.User;
import com.stydentsystem.repository.AppUserRepository;
import com.stydentsystem.repository.UserRepository;
import com.stydentsystem.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stydentsystem.exception.NotFoundException;
import com.stydentsystem.exception.DuplicateException;

import java.util.List;
/**
 * This is a class that provides implementation of the main functionality of the user.
 */
@Service
public class UserServiceImpl implements UserService {
    private UserRepository usersRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public UserServiceImpl(UserRepository usersRepository,
                            AppUserRepository appUserRepository) {
        this.usersRepository = usersRepository;
        this.appUserRepository = appUserRepository;
    }

    @Override
    public List<User> getAll() {
        return usersRepository.findAllByEnabledIsTrue();
    }

    @Override
    public User getByUsername(String username) throws NotFoundException {
        if (!usersRepository.existsByUsernameAndEnabledIsTrue(username)) {
            throw new NotFoundException("User", "username", username);
        }
        return usersRepository.findByUsernameAndEnabledIsTrue(username);
    }

    @Override
    public User getById(int id) throws NotFoundException {
        if (!usersRepository.existsByIdAndEnabledIsTrue(id)) {
            throw new NotFoundException("User", "ID", String.valueOf(id));
        }
        return usersRepository.findByIdAndEnabledIsTrue(id);
    }

    @Override
    public void createUser(User user, AppUser appUser) throws DuplicateException {
        if (usersRepository.existsByUsername(user.getUsername())) {
            throw new DuplicateException("User", "username", user.getUsername());
        }
        appUserRepository.saveAndFlush(appUser);
        usersRepository.saveAndFlush(user);
    }

    @Override
    public void updateUser(User user, AppUser appUser) throws NotFoundException {
        if (!usersRepository.existsByUsername(user.getUsername())) {
            throw new NotFoundException("User", "username", user.getUsername());
        }
        appUserRepository.saveAndFlush(appUser);
        usersRepository.saveAndFlush(user);
    }

    @Override
    public void deleteUserById(int id) throws NotFoundException {
        if (!usersRepository.existsById(id)) {
            throw new NotFoundException("User", "ID", String.valueOf(id));
        }
        User deletedUser = usersRepository.findByIdAndEnabledIsTrue(id);
        deletedUser.setEnabled(false);
        usersRepository.saveAndFlush(deletedUser);
    }

    @Override
    public void deleteUserByUsername(String username) throws NotFoundException {
        if(!usersRepository.existsByUsername(username)) {
            throw new NotFoundException("User", "username", String.valueOf(username));
        }
        User userToDelete = usersRepository.findByUsernameAndEnabledIsTrue(username);

        userToDelete.setEnabled(false);
        usersRepository.saveAndFlush(userToDelete);
    }
}


