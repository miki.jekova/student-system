package com.stydentsystem.service;

import com.stydentsystem.entity.Teacher;
import com.stydentsystem.repository.TeacherRepository;
import com.stydentsystem.service.contract.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * This is a class that provides implementation of the main functionality of the teacher.
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    private TeacherRepository teacherRepository;

    @Autowired
    public TeacherServiceImpl(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }
}
