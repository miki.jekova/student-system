package com.stydentsystem.service.contract;

import com.stydentsystem.entity.Student;

import java.util.List;

/**
 * This interface defines the contract that business functionality classes have to follow.
 */
public interface StudentService {

    List<Student> getAll();

}