package com.stydentsystem.service.contract;

import com.stydentsystem.entity.AppUser;
import com.stydentsystem.entity.User;
import com.stydentsystem.exception.DuplicateException;
import com.stydentsystem.exception.NotFoundException;

import java.util.List;
/**
 * This interface defines the contract that business functionality classes have to follow.
 */
public interface UserService {

    List<User> getAll();

    User getByUsername(String username) throws NotFoundException;

    User getById(int id) throws NotFoundException;

    void createUser(User user, AppUser appUser) throws DuplicateException;

    void updateUser(User user, AppUser appUser) throws NotFoundException;

    void deleteUserById(int id) throws NotFoundException, NotFoundException;

    void deleteUserByUsername(String username) throws NotFoundException;
}
