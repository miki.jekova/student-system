package com.stydentsystem.service.contract;

import com.stydentsystem.entity.Course;

import java.util.List;

/**
 * This interface defines the contract that business functionality classes have to follow.
 */
public interface CourseService {

    List<Course> getAll();

    Course getCourseById(int id);

}
