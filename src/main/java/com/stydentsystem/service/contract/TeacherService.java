package com.stydentsystem.service.contract;

import com.stydentsystem.entity.Teacher;

import java.util.List;

/**
 * This interface defines the contract that business functionality classes have to follow.
 */
public interface TeacherService {

    List<Teacher> getAll();

}
